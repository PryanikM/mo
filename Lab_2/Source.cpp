#include <iostream>
#include <functional>
#include <vector>

struct Point_2D
{
	double x_1;
	double x_2;
};

double obf_function(Point_2D point) {
	return ((point.x_1 - 3.) * (point.x_1 - 3.) + (point.x_2 - 5.) * (point.x_2 - 5.));
}

double H(const Point_2D& point) {
	return (point.x_2 - 2. * point.x_1 - 5);
}

double G(const Point_2D& point) {
	return point.x_1 * point.x_1 + point.x_2 * point.x_2 - 10;;
}

double penalty_function(Point_2D point,
	const double& penalty)
{
	return  H(point) * H(point) / (2 * penalty) - penalty / (G(point));
}

double P(const Point_2D& point,
	const double& r)
{
	return (obf_function(point) + penalty_function(point, r));
}

Point_2D gradient_obf_function(Point_2D point) {
	Point_2D gradient{ 2. * (point.x_1 - 3.), 2. * (point.x_2 - 5) };
	return gradient;
}

Point_2D gradient_H(Point_2D point) {
	Point_2D gradient{ -2, 1 };
	return gradient;

}

Point_2D gradient_G(Point_2D point) {
	Point_2D gradient{ 2 * point.x_1, 2 * point.x_2 };
	return gradient;

}

Point_2D gradient(Point_2D x, double r) {
	Point_2D gradient;

	Point_2D value_gradient_function = gradient_obf_function(x);
	Point_2D value_gradient_H = gradient_H(x);
	Point_2D value_gradient_G = gradient_G(x);

	gradient.x_1 = value_gradient_function.x_1 + H(x) * value_gradient_H.x_1 / r  + r * value_gradient_G.x_1 / (G(x) * G(x));
	gradient.x_2 = value_gradient_function.x_2 + H(x) * value_gradient_H.x_2 / r + r * value_gradient_G.x_2 / (G(x) * G(x));

	return gradient;

}

double Norma(Point_2D p)
{
	return sqrt(p.x_1 * p.x_1 + p.x_2 * p.x_2);
}	

void inverseHess(std::vector<std::vector<double>>& ih, Point_2D point, double r) {
	Point_2D H_der = gradient_H(point);
	Point_2D G_der = gradient_G(point);
	Point_2D f_der = gradient_obf_function(point);

	ih[0][0] = 2 + (H_der.x_1 * H_der.x_1) / r - 2 * r * (G_der.x_1 * G_der.x_1) / std::pow(G(point), 3) + 2 * r / std::pow(G(point), 2);
	ih[0][1] = H_der.x_1 * H_der.x_2 / r - 2 * r * G_der.x_1 * G_der.x_2 / std::pow(G(point), 3);
	ih[1][0] = H_der.x_1 * H_der.x_2 / r - 2 * r * G_der.x_1 * G_der.x_2 / std::pow(G(point), 3);
	ih[1][1] = 2 + H_der.x_2 * H_der.x_2 / r - 2 * r * G_der.x_2 * G_der.x_2 / std::pow(G(point), 3) + 2 * r / std::pow(G(point), 2);

	//std::cout << ih[0][0] << "\t";
	//std::cout << ih[0][1] << "\n";
	//std::cout << ih[1][0] << "\t";
	//std::cout << ih[1][1] << "\n";

	//std::cout << "x1 = " << point.x_1 << "  x2 = " << point.x_2 << "  r = " << r;
	//std::cout << "\n\n";

	//double h = 0.001;

	double det = ih[0][0] * ih[1][1] - ih[0][1] * ih[1][0];

	ih[0][0] = ih[1][1] / det;
	ih[0][1] = -ih[1][0] / det;
	ih[1][0] = -ih[0][1] / det;
	ih[1][1] = ih[0][0] / det;

}

Point_2D Newton(Point_2D current, double r, double epsilon = 0.001)
{
	int iter_count = 0;

	double norma = 0.;
	std::vector<std::vector<double>> hm(2, std::vector<double>(2, 0));

	//inverseHess(hm, current, r);
	Point_2D gradient_point = gradient(current, r);

	do
	{
		inverseHess(hm, current, r);
		for (int i = 0; i < 2; ++i) {
			std::cout << hm[i][0] << "  ";
			std::cout << hm[i][0] << "\n";
		}
		std::cout << "------\n";
		iter_count++;
		current.x_1 -= (hm[0][0] * gradient_point.x_1 + hm[0][1] * gradient_point.x_2);

		current.x_2 -= (hm[1][0] * gradient_point.x_1 + hm[1][1] * gradient_point.x_2);

		gradient_point = gradient(current, r);

		norma = Norma(gradient_point);

		//std::cout << "Norma = " << norma << '\n';

	} while ((norma > epsilon));

	std::cout << "Count of iterations of unconditional optimization methods: " << iter_count << std::endl;
	return current;	
}

int main() {

	double epsilon_1 = 0.1;
	double epsilon_2 = 0.1;

	double r = 10;
	double C = 2;

	Point_2D point_current;
	do
	{
		std::cout << "Input start point: " << '\n';
		std::cin >> point_current.x_1 >> point_current.x_2;
		std::cout << std::endl;
		if (G(point_current) >= 0) { std::cout << "Initial approximation doesn't satisfy the condition: g(x1,x2) < 0" << "\n\n"; }
	} while (G(point_current) >= 0);

	int k = 0;

	double penalty_func_result;
	do
	{
		k++;
		point_current = Newton(point_current, r, epsilon_2);
		//point_current = Gradient_With_Part(point_current, r);
		penalty_func_result = penalty_function(point_current, r);
		r = r / C;
		std::cout << penalty_func_result << '\n';
	} while (abs(penalty_func_result) > epsilon_1);

	std::cout << "Solution is " << point_current.x_1 << ", " << point_current.x_2 << std::endl;
	


	return 0;
}