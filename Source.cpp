#include <iostream>
#include <vector>

struct Point
{
	double x1;
	double x2;
};

double function(Point x) {
	return (x.x1 + x.x2) * (x.x1 + x.x2) + (x.x2 + 6.) * (x.x2 + 6.);
}


void inverseHess(std::vector<std::vector<double>> &ih, Point point) {
	ih[0][0] = 2;
	ih[0][1] = 2;
	ih[1][0] = 2;
	ih[1][1] = 4;

	double h = 0.01;

	double det = ih[0][0] * ih[1][1] - ih[0][1] * ih[1][0];

	ih[0][0] = ih[1][1] / det;
	ih[0][1] = -ih[1][0] / det;
	ih[1][0] = -ih[0][1] / det;
	ih[1][1] = ih[0][0] / det;

}

Point gradient(Point x) {
	Point gradient_p;
	gradient_p.x1 = 2. * (x.x1 + x.x2);
	gradient_p.x2 = 2. * x.x1 + 4. * x.x2 + 12;
	return gradient_p;

}
double Y(double a, double b)
{
	return (a + (3 - sqrt(5)) * (b - a) / 2);
}
double Z(double a, double b)
{
	return (a + (sqrt(5) - 1) * (b - a) / 2);
}

double Norma(Point point)
{
	return sqrt(point.x1 * point.x1 + point.x2 * point.x2);
}
 

double GoldenRatio(Point point_grad, Point point)
{
	double a = -10.0, b = 10.0;
	double y, z;
	double eps = 0.01;

	do
	{
		y = Y(a, b); z = Z(a, b);
		Point newPoint_1{ point.x1 + y * point_grad.x1, point.x2 + y * point_grad.x2 };
		Point newPoint_2{ point.x1 + z * point_grad.x1, point.x2 + z * point_grad.x2 };
		if (function(newPoint_1) <= function(newPoint_2))
		{
			b = z;
			z = y;
			y = Y(a, b);
		}
		else
		{
			a = y;
			y = z;
			z = Z(a, b);
		}
		newPoint_1.x1 = point.x1 + y * point.x1;
		newPoint_1.x2 = point.x2 + y * point.x2;
		
		newPoint_2.x1 = point.x1 + z * point.x1;
		newPoint_2.x2 = point.x2 + z * point.x2;

	} while (abs(b - a) > eps);

	return ((a + b) / 2);
}


double method_steepest_descent (Point x0, int iter_max=500, double epsilon=0.01){
	int iter_count = 0;
	Point current = x0;
	double l = 0.;
	double norma = 0.;
	Point last;
	Point gradient_point = gradient(current);
	l = GoldenRatio(gradient_point, current);

	do
	{
		iter_count++;
		current.x1 = current.x1 + l * gradient_point.x1;
		current.x2 = current.x2 + l * gradient_point.x2;

		gradient_point = gradient(current);
		l = GoldenRatio(gradient_point, current);

		norma = Norma(gradient_point);

	} while ((norma > epsilon) && (iter_count < iter_max));

	std::cout << "Speedy Descent: " << std::endl;
	std::cout << "Count of iteration: " << iter_count << std::endl;
	std::cout << "Solution is " << current.x1 << ", " << current.x2 << std::endl;
	std::cout << std::endl << std::endl;
	return 0.;
}

void Newton(Point current, int iter_max=500, double epsilon=0.01)
{
	int iter_count = 0;

	double norma = 0.;
	std::vector<std::vector<double>> hm(2, std::vector<double>(2, 0));

	inverseHess(hm, current);
	Point gradient_point = gradient(current);

	do
	{
		iter_count++;
		current.x1 -= (hm[0][0] * gradient_point.x1 + hm[0][1] * gradient_point.x2);

		current.x2 -= (hm[1][0] * gradient_point.x1 + hm[1][1] * gradient_point.x2);

		gradient_point = gradient(current);

		norma = Norma(gradient_point);

	} while ((norma > epsilon) && (iter_count < iter_max));

	std::cout << "Newton: " << std::endl;
	std::cout << "Count of iteration: " << iter_count << std::endl;
	std::cout << "Solution is " << current.x1 << ", " << current.x2 << std::endl;
}


int main() {

	method_steepest_descent(Point{ -0, 0 });

	Newton(Point{ -100., 100. });
	return 0;
}