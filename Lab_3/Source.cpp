#include <iostream>
#include <iomanip>

using namespace std;

int main()

{
    cout.precision(7);

    double matrix[5][5];

    matrix[0][0] = 1.01;		// ������
    matrix[0][1] = 1.01;		// �����
    matrix[0][2] = 9.45;		// �������
    matrix[0][3] = 0.3;		    
    matrix[0][4] = 14000.0;		// �������� ���

    matrix[1][0] = 1.0 / 500.0;
    matrix[1][1] = 1.0 / 600.0;
    matrix[1][2] = 0.0;
    matrix[1][3] = 1. / 600.;
    matrix[1][4] = 21.0;

    matrix[2][0] = 0.0;
    matrix[2][1] = 0.0;
    matrix[2][2] = 1.0 / 30.0;
    matrix[2][3] = 0.0;
    matrix[2][4] = 16.0;

    matrix[3][0] = 0.0;
    matrix[3][1] = 1.0;
    matrix[3][2] = 0.0;
    matrix[3][3] = 1.;
    matrix[3][4] = 4000.0;

    matrix[4][0] = -24.;
    matrix[4][1] = -27.;
    matrix[4][2] = -138.;
    matrix[4][3] = -7.7;
    matrix[4][4] = 0.0;

    double new_matrix[5][5];

    do {
        double min_column = 0.0;

        int min_value_column;

        for (int j = 0; j <= 4; j++) {
            if (matrix[4][j] < min_column) {
                min_column = matrix[4][j];
                min_value_column = j;
            }
        }

        cout << "#" << min_value_column << '\t' << min_column << " (min column)" << endl;

        double min_row = 10000000000.0;

        int min_value_row;

        for (int i = 0; i <= 4; i++) {
            if (matrix[i][min_value_column] > 0.0 && matrix[i][4] / matrix[i][min_value_column] < min_row) {
                min_row = matrix[i][4];
                min_value_row = i;
            }
        }
        cout << "#" << min_value_row << '\t' << min_row << " (min row)" << endl;

        double razr = matrix[min_value_row][min_value_column];
        cout << "razr = " << razr << endl;

        for (int i = 0; i <= 4; i++) {
            for (int j = 0; j <= 4; j++) {
                new_matrix[i][j] = (razr * matrix[i][j] - matrix[min_value_row][j] * matrix[i][min_value_column]) / razr;
            }
        }

        for (int i = 0; i <= 4; i++) {
            new_matrix[i][min_value_column] = -matrix[i][min_value_column] / razr;
        }

        for (int j = 0; j <= 4; j++) {
            new_matrix[min_value_row][j] = matrix[min_value_row][j] / razr;
        }

        new_matrix[min_value_row][min_value_column] = 1 / razr;

        for (int i = 0; i <= 4; i++) {
            for (int j = 0; j <= 4; j++) {
                matrix[i][j] = new_matrix[i][j];
            }
        }

        cout << endl;

        for (int i = 0; i <= 4; i++) {
            for (int j = 0; j <= 4; j++) {
                cout << "(" << i << ", " << j << ") " << matrix[i][j] << '\t';
            }
            cout << endl;
        }
        cout << endl;
    } while ((matrix[4][0] <= 0.0) || (matrix[4][1] <= 0.0) || (matrix[4][2] <= 0.0) || (matrix[4][3] <= 0.0));

    double max_value = 0.0;
    for (int i = 0; i <= 5; i++) {
        if (matrix[i][4] > max_value)
            max_value = matrix[i][4];
    }
    double g;
    for (int i = 0; i <= 4; i++) {
        g = matrix[i][4] - max_value;
        //matrix[i][4] = g;
    }

    cout << endl << "Result: " << endl;
    cout << "M = " << matrix[0][4] << endl;
    cout << "K = " << matrix[1][4] << endl;
    cout << "C = " << matrix[2][4] << endl;
    cout << "T = " << matrix[3][4] << endl;
    cout << "Max Profit = " << matrix[4][4] << endl << endl;

    return 0;
}